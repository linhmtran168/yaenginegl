package com.androidgame.framework;

import com.androidgame.framework.Input.TouchEvent;

/**
 * The game screen abstract class. Its children class's instance implement the
 * game's screens what loaded in them (graphic, audio,...)
 * 
 * @author TheEmperor
 * 
 */
public abstract class Screen {
	/**
	 * The main game instance
	 */
	protected final Game game;

	public Screen(Game game) {
		this.game = game;
	}

	/**
	 * Update the current screen
	 * 
	 * @param deltaTime
	 *            float time passed between 2 frames
	 */
	public abstract void update(float deltaTime);

	/**
	 * Check if the touch event is in a specific area or not
	 * 
	 * @param event
	 *            TouchEvent the touch event
	 * @param x
	 *            Integer top left x coordinate of area
	 * @param y
	 *            Integer top left y coordinate of the area
	 * @param width
	 *            Integer width of the area
	 * @param height
	 *            Integer height of the area
	 * @return Boolean
	 */
	protected boolean inBounds(TouchEvent event, int x, int y, int width,
			int height) {
		if (event.x > x && event.x < x + width - 1 && event.y > y
				&& event.y < y + height - 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Present the current screen (draw graphic, play audio file...)
	 * 
	 * @param deltaTime
	 */
	public abstract void present(float deltaTime);

	/**
	 * Called when the game pause
	 */
	public abstract void pause();

	/**
	 * Called when the game resumed
	 */
	public abstract void resume();

	/**
	 * Dispose the current screen via game.setScreen() and release resources
	 */
	public abstract void dispose();
}
