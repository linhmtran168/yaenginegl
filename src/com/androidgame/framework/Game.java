/**
 * 
 */
package com.androidgame.framework;

/**
 * Implement the main framework interface
 * 
 * @author TheEmperor
 * 
 */
public interface Game {
	/**
	 * @return Input An instance of the module handling input
	 */
	public Input getInput();

	/**
	 * @return FileIO An instance of the module handling FileIO
	 */
	public FileIO getFileIO();

	/**
	 * @return Audio An instance of the module handling audio
	 */
	public Audio getAudio();

	/**
	 * @return Graphic An instance of the module handling graphics
	 */
	public Graphic getGraphics();

	/**
	 * Set the current Screen of the game
	 * 
	 * @param screen
	 */
	public void setScreen(Screen screen);

	/**
	 * @return Screen the currently active Screen
	 */
	public Screen getCurrentScreen();

	/**
	 * @return Screen an instance of the first screen of the game
	 */
	public Screen getStartScreen();
}
