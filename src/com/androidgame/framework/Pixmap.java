/**
 * 
 */
package com.androidgame.framework;

import com.androidgame.framework.Graphic.PixmapFormat;

/**
 * @author TheEmperor
 * 
 */
public interface Pixmap {
	/**
	 * @return Integer the width of the Pixmap in pixel
	 */
	public int getWidth();

	/**
	 * @return Integer the heigth of the Pixmap in pixel
	 */
	public int getHeight();

	/**
	 * @return PixmapFormat the format of the Pixmap stored in RAM
	 */
	public PixmapFormat getFormat();

	/**
	 * Dispose the Pixmap instance in the memory
	 */
	public void dispose();
}
