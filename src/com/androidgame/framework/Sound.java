package com.androidgame.framework;

/**
 * Implement methods to handle the sound file loaded in the memory
 * 
 * @author TheEmperor
 * 
 */
public interface Sound {
	/**
	 * Play the audio file loaded in the memory
	 * 
	 * @param volume
	 */
	public void play(float volume);

	/**
	 * Release the audio file from the memory
	 */
	public void dispose();
}
