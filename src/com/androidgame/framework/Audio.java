package com.androidgame.framework;

/**
 * Interface implementing method creating audio related instances
 * 
 * @author TheEmperor
 * 
 */
public interface Audio {
	/**
	 * Streaming a music file and create a Music instance
	 * 
	 * @param filename
	 * @return Music
	 */
	public Music newMusic(String filename);

	/**
	 * Load a sound file to the memory and create a Sound instance
	 * 
	 * @param filename
	 * @return Sound
	 */
	public Sound newSound(String filename);

}
