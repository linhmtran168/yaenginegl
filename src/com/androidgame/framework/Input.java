/**
 * 
 */
package com.androidgame.framework;

import java.util.List;

/**
 * Interface provide method to get input from user including: - Polling
 * (touchscreen, keyboard, accelerometer) - Event-based handling (touchscreen,
 * keyboard)
 * 
 * @author TheEmperor
 */
public interface Input {
	/**
	 * Keyboard Event static member class
	 * 
	 * @author TheEmperor
	 * 
	 */
	public static class KeyboardEvent {
		public static final int KEY_DOWN = 0;
		public static final int KEY_UP = 1;

		public int type;
		public int keyCode;
		public char keyChar;
	}

	/**
	 * Touch Event static member class
	 * 
	 * @author TheEmperor
	 * 
	 */
	public static class TouchEvent {
		public static final int TOUCH_DOWN = 0;
		public static final int TOUCH_UP = 1;
		public static final int TOUCH_DRAGGED = 2;

		public int type;
		public int x, y;
		public int pointer;
	}

	/**
	 * Check if key is pressed or not
	 * 
	 * @param keyCode
	 * @return Boolean
	 */
	public boolean isKeyPressed(int keyCode);

	/**
	 * Check if the screen is touch down or not
	 * 
	 * @param pointer
	 * @return Boolean
	 */
	public boolean isTouchDown(int pointer);

	/**
	 * Get the X coordinate of the touch point
	 * 
	 * @param pointer
	 * @return Integer
	 */
	public int getTouchX(int pointer);

	/**
	 * Get the y coordinate of touch position
	 * 
	 * @param pointer
	 * @return Integer
	 */
	public int getTouchY(int pointer);

	/**
	 * Get x-axis' acceleration
	 * 
	 * @return float
	 */
	public float getAccelX();

	/**
	 * Get y-axis' acceleration
	 * 
	 * @return float
	 */
	public float getAccelY();

	/**
	 * Get z-axis' acceleration
	 * 
	 * @return float
	 */
	public float getAccelZ();

	/**
	 * Get the list of keyboard events
	 * 
	 * @return List
	 */
	public List<KeyboardEvent> getKeyEvents();

	/**
	 * Get the list of touch events
	 * 
	 * @return List
	 */
	public List<TouchEvent> getTouchEvents();

}
