package com.androidgame.framework;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface handling file input, output
 * 
 * @author TheEmperor
 */
public interface FileIO {
	/**
	 * Read game assets(image, audio...)
	 * 
	 * @param fileName
	 * @return InputStream
	 * @throws IOException
	 */
	public InputStream readAsset(String fileName) throws IOException;

	/**
	 * Read data from a file
	 * 
	 * @param fileName
	 * @return InputStream
	 * @throws IOException
	 */
	public InputStream readFile(String fileName) throws IOException;

	/**
	 * Write data to a file
	 * 
	 * @param fileName
	 * @return OutputStream
	 * @throws IOException
	 */
	public OutputStream writeFile(String fileName) throws IOException;

}
