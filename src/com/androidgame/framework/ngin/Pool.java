package com.androidgame.framework.ngin;

import java.util.ArrayList;
import java.util.List;

/**
 * To prevent garbage collection go bad, we use this class to implement the
 * concept "instance pooling". Instead of creating new instances of a class
 * frequently, we'll simply reuse previously created instances.
 * 
 * A generic type class (for keyboard, single touch event and multi-touch event)
 * 
 * @author TheEmperor
 * 
 * @param <T>
 */
public class Pool<T> {
	public interface PoolObjectFactory<T> {
		/**
		 * @return a new object that has the generic type of the
		 *         Pool/PoolObjectFactory
		 */
		public T createObject();
	}

	/**
	 * ArrayList that stores pooled objects (no longer be used)
	 */
	private final List<T> freeObjects;
	/**
	 * Use to generate new instances of the type the class holds
	 */
	private final PoolObjectFactory<T> factory;
	/**
	 * Maximum number of objects the Pool can hold
	 */
	private final int maxSize;

	public Pool(PoolObjectFactory<T> factory, int maxSize) {
		this.factory = factory;
		this.maxSize = maxSize;
		this.freeObjects = new ArrayList<T>(maxSize);
	}

	/**
	 * @return a brand-new instance of the type that the Pool holds or a pooled
	 *         instance in case there's one in freeObjects ArrayList
	 */
	public T newObject() {
		T object = null;

		if (freeObjects.size() == 0) {
			object = factory.createObject();
		} else {
			object = freeObjects.remove(freeObjects.size() - 1);
		}
		return object;
	}

	/**
	 * Reinsert objects that we no longer use to the pool
	 * 
	 * @param object
	 */
	public void free(T object) {
		if (freeObjects.size() < maxSize) {
			freeObjects.add(object);
		}
	}
}
