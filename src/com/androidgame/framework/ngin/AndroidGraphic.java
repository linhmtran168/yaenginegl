package com.androidgame.framework.ngin;

import java.io.IOException;
import java.io.InputStream;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;

import com.androidgame.framework.Graphic;
import com.androidgame.framework.Pixmap;

public class AndroidGraphic implements Graphic {
	/**
	 * Load Bitmap instances
	 */
	AssetManager assets;
	/**
	 * Virtual framebuffer
	 */
	Bitmap frameBuffer;
	/**
	 * Canvas for draw to the virtual framebuffer
	 */
	Canvas canvas;
	Paint paint;
	Typeface font;
	Rect srcRect = new Rect();
	Rect dstRect = new Rect();

	public AndroidGraphic(AssetManager assets, Bitmap frameBuffer) {
		this.assets = assets;
		this.frameBuffer = frameBuffer;
		this.canvas = new Canvas(frameBuffer);
		this.paint = new Paint();
	}

	@Override
	public Pixmap newPixmap(String fileName, PixmapFormat format) {
		Config config = null;
		if (format == PixmapFormat.RGB565) {
			config = Config.RGB_565;
		} else if (format == PixmapFormat.ARGB4444) {
			config = Config.ARGB_4444;
		} else {
			config = Config.ARGB_8888;
		}

		Options options = new Options();
		options.inPreferredConfig = config;

		InputStream in = null;
		Bitmap bitmap = null;
		try {
			in = assets.open(fileName);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			if (bitmap == null) {
				throw new RuntimeException("Couldn' load bitmap from asset '"
						+ fileName + "'");
			}
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load bitmap from asset '"
					+ fileName + "'");
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					//
				}
			}
		}

		// Rechecks the format that BitmapFactory decoded to
		if (bitmap.getConfig() == Config.RGB_565) {
			format = PixmapFormat.RGB565;
		} else if (bitmap.getConfig() == Config.ARGB_4444) {
			format = PixmapFormat.ARGB4444;
		} else {
			format = PixmapFormat.ARGB8888;
		}

		return new AndroidPixmap(bitmap, format);
	}

	@Override
	public void drawGraphicText(String text, int x, int y, int size, int align,
			boolean isBold, String font, String color) {
		paint.setColor(Color.parseColor(color));
		if (font != "") {
			Typeface fontType = Typeface.createFromAsset(assets, font);
			paint.setTypeface(fontType);
		}
		paint.setTextSize(size);
		paint.setFakeBoldText(isBold);
		switch (align) {
		case 0:
			paint.setTextAlign(Paint.Align.LEFT);
			break;

		case 1:
			paint.setTextAlign(Paint.Align.CENTER);
			break;

		case 2:
			paint.setTextAlign(Paint.Align.RIGHT);
			break;

		default:
			paint.setTextAlign(Paint.Align.LEFT);
			break;

		}

		canvas.drawText(text, x, y, paint);
	}

	@Override
	public void clear(int color) {
		canvas.drawRGB((color & 0xff000) >> 16, (color & 0xff00) >> 8,
				(color & 0xff));
	}

	@Override
	public void drawPixel(int x, int y, int color) {
		paint.setColor(color);
		canvas.drawPoint(x, y, paint);
	}

	@Override
	public void drawLine(int x1, int y1, int x2, int y2, int color) {
		paint.setColor(color);
		canvas.drawLine(x1, y1, x2, y2, paint);
	}

	@Override
	public void drawRect(int x, int y, int width, int height, int color) {
		paint.setColor(color);
		paint.setStyle(Style.FILL);
		canvas.drawRect(x, y, x + width - 1, y + height - 1, paint);
	}

	@Override
	public void drawPixmap(Pixmap pixmap, int x, int y, int srcX, int srcY,
			int srcWidth, int srcHeight) {
		srcRect.left = srcX;
		srcRect.top = srcY;
		srcRect.right = srcX + srcWidth - 1;
		srcRect.bottom = srcY + srcHeight - 1;

		dstRect.left = x;
		dstRect.top = y;
		dstRect.right = x + srcWidth - 1;
		dstRect.bottom = y + srcHeight - 1;

		canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, srcRect, dstRect,
				null);
	}

	@Override
	public void drawPixmap(Pixmap pixmap, int x, int y) {
		canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, x, y, null);
	}

	/**
	 * @return Integer the width of the virtual framebuffer
	 */
	@Override
	public int getWidth() {
		return frameBuffer.getWidth();
	}

	/**
	 * @return Integer the height of the virtual framebuffer
	 */
	@Override
	public int getHeight() {
		return frameBuffer.getHeight();
	}
}
