package com.androidgame.framework.ngin;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.androidgame.framework.Audio;
import com.androidgame.framework.Music;
import com.androidgame.framework.Sound;

/**
 * Class implement Audio interface and deal with Audio Handling: Load assets =>
 * createInstance of AndroidAudio and AndroidMusic => audio for the game
 * 
 * @author TheEmperor
 */
public class AndroidAudio implements Audio {
	/**
	 * Asset instance hold all audio file
	 */
	AssetManager assets;
	/**
	 * SoundPool instance to play audio file loaded in memory
	 */
	SoundPool soundPool;

	public AndroidAudio(Activity activity) {
		// Setting volume control
		activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		// Get assets from the activity
		this.assets = activity.getAssets();
		this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
	}

	@Override
	public Music newMusic(String fileName) {
		try {
			// Load audio file descriptor
			AssetFileDescriptor assetDescriptor = assets.openFd(fileName);
			return new AndroidMusic(assetDescriptor);
		} catch (IOException e) {
			throw new RuntimeException("Cound'n load music '" + fileName + "'");
		}
	}

	@Override
	public Sound newSound(String fileName) {
		try {
			AssetFileDescriptor assetDescriptor = assets.openFd(fileName);
			int soundId = soundPool.load(assetDescriptor, 0);
			return new AndroidSound(soundPool, soundId);
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load sound '" + fileName + ";");
		}
	}
}
