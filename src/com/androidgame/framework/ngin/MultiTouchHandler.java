package com.androidgame.framework.ngin;

import android.view.MotionEvent;
import android.view.View;

import com.androidgame.framework.Input.TouchEvent;

public class MultiTouchHandler extends MainTouchHandler {
	boolean[] isTouched = new boolean[20];
	int[] touchX = new int[20];
	int[] touchY = new int[20];

	public MultiTouchHandler(View view, float scaleX, float scaleY) {
		super(view, scaleX, scaleY);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		synchronized (this) {
			// Get type of action for this particular finger
			int action = event.getAction() & MotionEvent.ACTION_MASK;
			// Get finger pointer index
			int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
			// Get pointer id
			int pointerId = event.getPointerId(pointerIndex);
			TouchEvent touchEvent;

			switch (action) {
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
				touchEvent = touchEventPool.newObject();
				touchEvent.type = TouchEvent.TOUCH_DOWN;
				touchEvent.pointer = pointerId;
				touchEvent.x = touchX[pointerId] = (int) (event
						.getX(pointerIndex) * scaleX);
				touchEvent.y = touchY[pointerId] = (int) (event
						.getY(pointerIndex) * scaleY);
				isTouched[pointerId] = true;
				touchEventsBuffer.add(touchEvent);
				break;

			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
			case MotionEvent.ACTION_CANCEL:
				touchEvent = touchEventPool.newObject();
				touchEvent.type = TouchEvent.TOUCH_UP;
				touchEvent.x = touchX[pointerId] = (int) (event
						.getX(pointerIndex) * scaleX);
				touchEvent.y = touchY[pointerId] = (int) (event
						.getY(pointerIndex) * scaleY);
				isTouched[pointerId] = false;
				touchEventsBuffer.add(touchEvent);
				break;

			case MotionEvent.ACTION_MOVE:
				int pointerCount = event.getPointerCount();
				for (int i = 0; i < pointerCount; i++) {
					pointerIndex = i;
					pointerId = event.getPointerId(pointerIndex);

					touchEvent = touchEventPool.newObject();
					touchEvent.type = TouchEvent.TOUCH_DRAGGED;
					touchEvent.pointer = pointerId;
					touchEvent.x = touchX[pointerId] = (int) (event
							.getX(pointerIndex) * scaleX);
					touchEvent.y = touchY[pointerId] = (int) (event
							.getY(pointerIndex) * scaleY);
					touchEventsBuffer.add(touchEvent);
				}
				break;
			}
			return true;
		}
	}

	@Override
	public boolean isTouchDown(int pointer) {
		synchronized (this) {
			if (pointer < 0 || pointer >= 20) {
				return false;
			} else {
				return isTouched[pointer];
			}
		}
	}

	@Override
	public int getTouchX(int pointer) {
		synchronized (this) {
			if (pointer < 0 || pointer >= 20) {
				return 0;
			} else {
				return touchX[pointer];
			}
		}
	}

	@Override
	public int getTouchY(int pointer) {
		synchronized (this) {
			if (pointer < 0 || pointer >= 20) {
				return 0;
			} else {
				return touchY[pointer];
			}
		}
	}
}
