package com.androidgame.framework.ngin;

import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;
import android.view.View;

import com.androidgame.framework.Input.TouchEvent;
import com.androidgame.framework.ngin.Pool.PoolObjectFactory;

/**
 * Abstract class implement some of bookkeeping member and method for touch
 * events
 * 
 * @author TheEmperor
 * 
 */
public abstract class MainTouchHandler implements TouchHandler {
	/**
	 * Pool holds touch events
	 */
	Pool<TouchEvent> touchEventPool;
	/**
	 * Array holds events that are not processed
	 */
	List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();
	/**
	 * Hold processed events
	 */
	List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();
	/**
	 * Scaling factor for x coordinate of touch event (target/real resolution)
	 */
	float scaleX;
	/**
	 * Scaling factor for y coordinate of touch event (target/real resolution)
	 */
	float scaleY;

	public MainTouchHandler(View view, float scaleX, float scaleY) {
		PoolObjectFactory<TouchEvent> factory = new PoolObjectFactory<TouchEvent>() {
			@Override
			public TouchEvent createObject() {
				return new TouchEvent();
			}
		};

		touchEventPool = new Pool<TouchEvent>(factory, 100);
		view.setOnTouchListener(this);
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}

	@Override
	public abstract boolean onTouch(View v, MotionEvent event);

	@Override
	public abstract boolean isTouchDown(int pointer);

	@Override
	public abstract int getTouchX(int pointer);

	@Override
	public abstract int getTouchY(int pointer);

	/**
	 * Return list of touch events Called on main loop thread
	 */
	@Override
	public List<TouchEvent> getTouchEvents() {
		synchronized (this) {
			int len = touchEvents.size();
			for (int i = 0; i < len; i++) {
				touchEventPool.free(touchEvents.get(i));
			}
			touchEvents.clear();
			touchEvents.addAll(touchEventsBuffer);
			touchEventsBuffer.clear();
			return touchEvents;
		}
	}
}
