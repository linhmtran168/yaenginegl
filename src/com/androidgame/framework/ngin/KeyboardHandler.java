package com.androidgame.framework.ngin;

import java.util.ArrayList;
import java.util.List;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;

import com.androidgame.framework.Input.KeyboardEvent;
import com.androidgame.framework.ngin.Pool.PoolObjectFactory;

public class KeyboardHandler implements OnKeyListener {
	/**
	 * Store the current state of each key in the keyboards
	 */
	boolean[] pressedKeys = new boolean[128];
	/**
	 * A pool hold instances of the class
	 */
	Pool<KeyboardEvent> keyEventPool;
	/**
	 * Hold key events that have not yet be consumed by the game
	 */
	List<KeyboardEvent> keyEventsBuffer = new ArrayList<KeyboardEvent>();
	/**
	 * Key events that returned in the method getKeyEvents()
	 */
	List<KeyboardEvent> keyEvents = new ArrayList<KeyboardEvent>();

	public KeyboardHandler(View view) {
		PoolObjectFactory<KeyboardEvent> factory = new PoolObjectFactory<KeyboardEvent>() {
			@Override
			public KeyboardEvent createObject() {
				return new KeyboardEvent();
			}
		};
		keyEventPool = new Pool<KeyboardEvent>(factory, 100);
		view.setOnKeyListener(this);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
	}

	/**
	 * Key event handler Called on UI Thread
	 */
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == android.view.KeyEvent.ACTION_MULTIPLE) {
			return false;
		}

		synchronized (this) {
			KeyboardEvent keyboardEvent = keyEventPool.newObject();
			keyboardEvent.keyCode = keyCode;
			keyboardEvent.keyChar = (char) event.getUnicodeChar();
			if (event.getAction() == android.view.KeyEvent.ACTION_DOWN) {
				keyboardEvent.type = KeyboardEvent.KEY_DOWN;
				if (keyCode > 0 && keyCode < 127) {
					pressedKeys[keyCode] = true;
				}
			}

			if (event.getAction() == android.view.KeyEvent.ACTION_UP) {
				keyboardEvent.type = KeyboardEvent.KEY_UP;
				if (keyCode > 0 && keyCode < 127) {
					pressedKeys[keyCode] = false;
				}
			}
			keyEventsBuffer.add(keyboardEvent);
		}
		return false;
	}

	public boolean isKeyPressed(int keyCode) {
		synchronized (this) {
			if (keyCode < 0 || keyCode > 127) {
				return false;
			}
			return pressedKeys[keyCode];
		}
	}

	/**
	 * Return the list of key events that consumed by the game Called on main
	 * loop thread
	 * 
	 * @return
	 */
	public List<KeyboardEvent> getKeyEvents() {
		synchronized (this) {
			int len = keyEvents.size();
			for (int i = 0; i < len; i++) {
				keyEventPool.free(keyEvents.get(i));
			}
			keyEvents.clear();
			keyEvents.addAll(keyEventsBuffer);
			keyEventsBuffer.clear();
			return keyEvents;
		}
	}
}
