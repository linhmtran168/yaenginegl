package com.androidgame.framework;

public interface Graphic {
	public static enum PixmapFormat {
		ARGB8888, ARGB4444, RGB565
	}

	/**
	 * Load an image to the memory
	 * 
	 * @param fileName
	 * @param format
	 * @return Pixmap the pixmap instance contains pixels of the image
	 */
	public Pixmap newPixmap(String fileName, PixmapFormat format);

	/**
	 * Clear the complete frame buffer with the given color
	 * 
	 * @param color
	 */
	public void clear(int color);

	/**
	 * Draw a given text with given position, font and color
	 * 
	 * @param text
	 * @param x
	 * @param y
	 * @param size
	 * @param align
	 * @param font
	 * @param color
	 */
	public void drawGraphicText(String text, int x, int y, int size, int align,
			boolean isBold, String font, String color);

	/**
	 * Set the pixel at (x,y) in the framebuffer to the given color
	 * 
	 * @param x
	 *            pixel's x coordinate
	 * @param y
	 *            pixel's y coordinate
	 * @param color
	 */
	public void drawPixel(int x, int y, int color);

	/**
	 * Draw a line from (x, y) to (x2, y2) with the given color
	 * 
	 * @param x
	 * @param y
	 * @param x2
	 * @param y2
	 * @param color
	 */
	public void drawLine(int x1, int y1, int x2, int y2, int color);

	/**
	 * Draw a rectangle to the framebuffer with the given color
	 * 
	 * @param x
	 *            x coordinate of rectangle's top-left corner position
	 * @param y
	 *            y coordinate of rectangle's top-left corner position
	 * @param width
	 * @param height
	 * @param color
	 */
	public void drawRect(int x, int y, int width, int height, int color);

	/**
	 * Draws rectangular portion of a Pixmap to the frame buffer
	 * 
	 * @param pixmap
	 * @param x
	 *            x coordinate of the top-left corner position of Pixmap's
	 *            target location
	 * @param y
	 *            y coordinate of the top-left corner position of Pixmap's
	 *            target location
	 * @param srcX
	 *            X coordinate of the top-left corner position of the rectangle
	 *            region used
	 * @param srcY
	 *            Y coordinate of the top-left corner position of the rectangle
	 *            region used
	 * @param srcWidth
	 *            rectangular portion's width
	 * @param srcHeight
	 *            rectangular portion's height
	 */
	public void drawPixmap(Pixmap pixmap, int x, int y, int srcX, int srcY,
			int srcWidth, int srcHeight);

	/**
	 * Draw a complete pixmap to the frame buffer
	 * 
	 * @param pixmap
	 * @param x
	 *            x coordinate of the top-left position of target location
	 * @param y
	 *            y coordinate of the top-left position of target location
	 */
	public void drawPixmap(Pixmap pixmap, int x, int y);

	/**
	 * @return Integer The width of the framebuffer in pixels
	 */
	public int getWidth();

	/**
	 * @return Integer The height of the framebuffer in pixels
	 */
	public int getHeight();
}
